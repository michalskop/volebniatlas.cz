
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Volební atlas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Detailní výsledky voleb detailní výsledky voleb na interaktivních mapách po volebních obvodech.' },
      { hid: 'og:name', property: 'og:image', content: "/illustration.png"},
      { hid: 'og:url', property: 'og:url', content: "https://volebniatlas.cz/"},
      { hid: 'og:type', property: 'og:type', content: 'website'},
      { hid: 'og:title', property: 'og:title', content: "Volební atlas"},
      { hid: 'og:description', property: 'og:description', content: 'Detailní výsledky voleb na interaktivních mapách po volebních obvodech.'},
      { hid: 'fb:app_id', property: 'fb:app_id', content: '300369678085355'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/custom.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    'nuxt-leaflet',
    '@nuxtjs/axios',
    '@nuxt/content',
    ['nuxt-matomo', { matomoUrl: '//piwik.kohovolit.eu/', siteId: 11 }],
  ],
  axios: {
    // proxyHeaders: false
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  },
  env: {

  }
}
