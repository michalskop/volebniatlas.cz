# O projektu Volební atlas

Volební atlas je souborem map s detailními výsledky voleb v ČR od roku 2013 podle volebních obvodů - krajů a senátních obvodů.

V současné době jde o **beta verzi**.

### Základní pomůcka pro volební štáby

Aneb detailní přehled o voličském chovování na úrovni okrsků.

Pokud o ně máte zájem, napište nám zprávu dolů do formuláře na **připomínky**.